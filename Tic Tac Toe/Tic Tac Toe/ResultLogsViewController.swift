//
//  ResultLogsViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs on 2/10/15.
//  Copyright (c) 2015 Deepak. All rights reserved.
//

import UIKit


class ResultLogsViewController: UIViewController, UITableViewDelegate{
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  @IBOutlet weak var resultLogTable: UITableView!
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return winnerName.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
    cell.textLabel?.adjustsFontSizeToFitWidth = true
    cell.textLabel?.numberOfLines = 5
    cell.textLabel?.text =  "\(playerOneNameArray[indexPath.row]) v/s \(playerTwoNameArray[indexPath.row]) \nWinner: \(winnerName[indexPath.row]) \nWon: \(matchesWon[indexPath.row]) \nTies:\(matchesTie[indexPath.row]) \nTotal Matches: \(totalMatches[indexPath.row]) "
    cell.textLabel?.sizeToFit()
    return cell
  }
  
  override func viewWillAppear(animated: Bool) {
    /* Updating the arra with the user default values.
    First initial an array with and set its values to the userdeault array usig the key.
    Emty the array is used to update the value of cells.
    Finally update its values using in for loop */
    if var persistentPlayerOne: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("PO") {
      playerOneNameArray = []
      for index in 0..<persistentPlayerOne.count {
        playerOneNameArray.append(persistentPlayerOne[index] as NSString)
      }
    }
    if var persistentPlayerTwo: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("PT") {
      playerTwoNameArray = []
      for index in 0..<persistentPlayerTwo.count {
        playerTwoNameArray.append(persistentPlayerTwo[index] as NSString)
        println(playerTwoNameArray)
      }
    }
    if var persistentWinnerArray: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("WN") {
      winnerName = [] // Emptying the toDoArray
      for index in 0..<persistentWinnerArray.count {
        winnerName.append(persistentWinnerArray[index] as NSString)
        println(winnerName)
      }
    }
    if var persistentWinArray: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("MW") {
      matchesWon = [] // Emptying the toDoArray
      for index in 0..<persistentWinArray.count {
        matchesWon.append(persistentWinArray[index] as Int)
        println(matchesWon)
      }
    }
    if var persistentTieArray: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("MT") {
      matchesTie = []
      for index in 0..<persistentTieArray.count {
        matchesTie.append(persistentTieArray[index] as Int)
        println(matchesTie)
      }
    }
    if var persistentTotalArray: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("TM") {
      totalMatches = []
      for index in 0..<persistentTotalArray.count {
        totalMatches.append(persistentTotalArray[index] as Int)
        println(totalMatches)
      }
    }
  }
  }
