//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs on 2/2/15.
//  Copyright (c) 2015 Deepak. All rights reserved.
//

import UIKit

var firstPlayerName = ""
var secondPlayerName = "Player Two"
var zeroImage = "circle1.png"
var crossImage = "cross1.png"
var boardImage = "ttt_board.png"
var playerOneWinCount = 0
var playerTwoWinCount = 0
var tiesCount = 0
var goNumber = 1
var goNumberTracker = 1
var playerOneNameArray = [String]()
var playerTwoNameArray = [String]()
var winnerName = [String]()
var matchesWon = [Int]()
var matchesTie = [Int]()
var totalMatches = [Int]()

class ViewController: UIViewController {
  var winner = 0 // 0 = empty, 1 = zero, 2 = cross
  var gameStage = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  let winningCombinations = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
  var count = 0
  
  @IBOutlet weak var resultLabel: UILabel!
  @IBOutlet weak var boardImageView: UIImageView!
  @IBOutlet weak var playagainOutlet: UIButton!
  
  @IBAction func buttonPressed(sender: AnyObject) {
    /*  Here when the buttons are pressed cross and zero are printed based on the value of goNumber.
    The value od winner is updated in the for loop.
    0 = empty, 1 = zero, 2 = cross
    According to the value of winner, the message on the result label is printed.
    */
    count++
    if (gameStage[sender.tag] == 0 && winner == 0) {
      var image = UIImage()
      if (goNumber%2==0){
        gameStage[sender.tag] = 1
        image = UIImage(named: crossImage)!
      } else {
        gameStage[sender.tag] = 2
        image = UIImage(named: zeroImage)!
      }
      for combination in winningCombinations{
        if (gameStage[combination[0]] == gameStage[combination[1]] && gameStage[combination[1]] == gameStage[combination[2]] && gameStage[combination[0]] != 0) {
          winner = gameStage[combination[0]]
        }
      }
      if winner != 0 {
        if winner == 1 {
          resultLabel.text = "Congratulations! \n" + firstPlayerName + " has won"
          playerOneWinCount++
          count = 0
        } else {
          resultLabel.text = "Congratulations! \n" + secondPlayerName + " has won"
          playerTwoWinCount++
          count = 0
        }
        UIView.animateWithDuration(0.5, animations: {
          self.resultLabel.alpha = 1
          self.playagainOutlet.alpha = 1
        })
      }else if winner == 0  && count == 9 {
        resultLabel.text = "Its a Draw \nTry Again"
        tiesCount++
        UIView.animateWithDuration(0.5, animations: {
          self.resultLabel.alpha = 1
          self.playagainOutlet.alpha = 1
        })
      }
      goNumber++
      sender.setBackgroundImage(image, forState: .Normal)
    }
  }
  
  @IBAction func playAgainAction(sender: AnyObject) {
    
    /* The goNumberTracker is used to keep the track of whether Zero goes first or Cross goes first.
    All the variables are initialised to initial values.
    for loop is used to set button images to nil. */
    if goNumberTracker == 1 {goNumber = 1}
    else if goNumberTracker == 2 {goNumber = 2}
    winner = 0
    gameStage = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    resultLabel.alpha = 0
    playagainOutlet.alpha = 0
    
    for index in 0..<9 {
      var button = UIButton()
      button = view.viewWithTag(index) as UIButton
      button.setBackgroundImage(nil, forState: .Normal)
    }
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    var image = UIImage(named: boardImage)
    boardImageView.image = image
    
    if goNumberTracker == 1 {goNumber = 1}
    else if goNumberTracker == 2 {goNumber = 2}
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewDidAppear(animated: Bool) {
    self.resultLabel.alpha = 0
    self.playagainOutlet.alpha = 0
  }
}

