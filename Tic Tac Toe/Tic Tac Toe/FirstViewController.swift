//
//  FirstViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs on 2/4/15.
//  Copyright (c) 2015 Deepak. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate {
  
  var isOptionOneViewvisible = false
  var isOptionTwoViewvisible = false
  
  @IBOutlet weak var playerOneNameField: UITextField!
  @IBOutlet weak var playerTwoNameField: UITextField!
  
  @IBOutlet weak var playerOneOptionOutlet: UIButton!
  @IBOutlet weak var playerTwoOptionOutlet: UIButton!
  
  @IBOutlet weak var playerOneOptionView: UIView!
  @IBOutlet weak var playerTwoOptionView: UIView!
  
  @IBAction func playerOneOptionPressed(sender: AnyObject) {
    // This animation is used to push the button and the option view left and right
    if isOptionOneViewvisible == false {
      self.playerOneOptionView.center = CGPointMake(self.playerOneOptionView.center.x + 400, self.playerOneOptionView.center.y)
      UIView.animateWithDuration(0.3, animations: {
        self.playerOneOptionView.center = CGPointMake(self.playerOneOptionView.center.x - 400, self.playerOneOptionView.center.y)
        self.playerOneOptionOutlet.center = CGPointMake(self.playerOneOptionOutlet.center.x - self.playerOneOptionView.bounds.width,self.playerOneOptionOutlet.center.y)
        self.playerOneOptionView.hidden = false
      })
      isOptionOneViewvisible = true
    } else if isOptionOneViewvisible == true {
      self.playerOneOptionView.center = CGPointMake(self.playerOneOptionView.center.x - 400, self.playerOneOptionView.center.y)
      UIView.animateWithDuration(0.3, animations: {
        self.playerOneOptionView.center = CGPointMake(self.playerOneOptionView.center.x + 400, self.playerOneOptionView.center.y)
        self.playerOneOptionOutlet.center = CGPointMake(self.playerOneOptionOutlet.center.x + self.playerOneOptionView.bounds.width,self.playerOneOptionOutlet.center.y)
        self.playerOneOptionView.hidden = true
      })
      isOptionOneViewvisible = false
    }
  }
  
  @IBAction func playerTwoOptionPressed(sender: AnyObject) {
    // This animation is used to push the button and the option view left and right
    if isOptionTwoViewvisible == false {
      self.playerTwoOptionView.center = CGPointMake(self.playerTwoOptionView.center.x + 400, self.playerTwoOptionView.center.y)
      UIView.animateWithDuration(0.3, animations: {
        self.playerTwoOptionView.center = CGPointMake(self.playerTwoOptionView.center.x - 400, self.playerTwoOptionView.center.y)
        self.playerTwoOptionOutlet.center = CGPointMake(self.playerTwoOptionOutlet.center.x - self.self.playerTwoOptionView.bounds.width, self.playerTwoOptionOutlet.center.y)
        self.playerTwoOptionView.hidden = false
      })
      isOptionTwoViewvisible = true
    } else if isOptionTwoViewvisible == true{
      self.playerTwoOptionView.center = CGPointMake(self.playerTwoOptionView.center.x - 400, self.playerTwoOptionView.center.y)
      UIView.animateWithDuration(0.3, animations: {
        self.playerTwoOptionView.center = CGPointMake(self.playerTwoOptionView.center.x + 400, self.playerTwoOptionView.center.y)
        self.playerTwoOptionOutlet.center = CGPointMake(self.playerTwoOptionOutlet.center.x + self.self.playerTwoOptionView.bounds.width, self.playerTwoOptionOutlet.center.y)
        self.playerTwoOptionView.hidden = true
      })
      isOptionTwoViewvisible = false
    }
  }
  
  @IBAction func zeroImagePressed(sender: AnyObject) {
    if sender.tag == 100 {zeroImage = "circle1.png"}
    if sender.tag == 101 {zeroImage = "circle2.png"}
    if sender.tag == 102 {zeroImage = "circle3.png"}
  }
  @IBAction func crossImagePressed(sender: AnyObject) {
    if sender.tag == 110 {crossImage = "cross1.png"}
    if sender.tag == 111 {crossImage = "cross2.png"}
    if sender.tag == 112 {crossImage = "cross3.png"}
  }
  @IBAction func choseBoardPressed(sender: AnyObject) {
    if sender.tag == 220 {boardImage = "ttt_board.png"}
    if sender.tag == 221 {boardImage = "board1.png"}
    if sender.tag == 222 {boardImage = "board2.png"}
  }
  @IBOutlet weak var choseZeroOutlet: UIButton!
  @IBAction func choseZeroFirstPressed(sender: AnyObject) {
    goNumber = 1
    goNumberTracker = 1
    choseZeroOutlet.backgroundColor = UIColor.orangeColor()
    choseCroseOutlet.backgroundColor = UIColor.yellowColor()
  }
  
  @IBOutlet weak var choseCroseOutlet: UIButton!
  @IBAction func choseCrossFirstPressed(sender: AnyObject) {
    goNumber = 2
    goNumberTracker = 2
    choseZeroOutlet.backgroundColor = UIColor.yellowColor()
    choseCroseOutlet.backgroundColor = UIColor.orangeColor()
  }
  
  @IBAction func playButtonPressed(sender: AnyObject) {
    if playerOneNameField.text != "" {
      firstPlayerName = playerOneNameField.text!
    } else if firstPlayerName == "" {
      firstPlayerName = "Player One"
    }
    if playerTwoNameField.text != "" {
      secondPlayerName = playerTwoNameField.text!
    } else {
      secondPlayerName = "Player Two"
    }
  }
  
  @IBAction func continueButtonPressed(sender: AnyObject) {
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.playerOneNameField.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func textFieldShouldClear(textField: UITextField) -> Bool { // This function is invoked when clear button on keyboard is pressed
    playerOneNameField.resignFirstResponder()
    playerTwoNameField.resignFirstResponder()
    return true
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { // This function is invoked when user touches anywhere on screen
    self.view.endEditing(true) // Hide the key board
  }
  
  override func supportedInterfaceOrientations() -> Int {
    return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
  }
}
