//
//  ThirdViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs on 2/4/15.
//  Copyright (c) 2015 Deepak. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
  
  @IBOutlet weak var firstPlayerNameLabel: UILabel!
  @IBOutlet weak var secondPlayerNameLabel: UILabel!
  @IBOutlet weak var totalLabel: UILabel!
  @IBOutlet weak var firstPlayerScoreLabel: UILabel!
  @IBOutlet weak var secondPlayerScoreLabel: UILabel!
  @IBOutlet weak var tiesLabel: UILabel!
  @IBOutlet weak var totalScoreLabel: UILabel!
  
  var totalScore = playerOneWinCount + playerTwoWinCount + tiesCount
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if firstPlayerName == "" {
      firstPlayerName = "Player One"
    }
    if secondPlayerName == "" {
      secondPlayerName = "Player Two"
    }
    self.firstPlayerNameLabel.text = firstPlayerName
    self.secondPlayerNameLabel.text = secondPlayerName
    self.firstPlayerScoreLabel.text = "\(playerOneWinCount)"
    self.secondPlayerScoreLabel.text = "\(playerTwoWinCount)"
    self.tiesLabel.text = "\(tiesCount)"
    self.totalScoreLabel.text = "\(totalScore)"
  }
  
  @IBAction func saveToLogsPressed(sender: AnyObject) {
    if playerOneWinCount > playerTwoWinCount {
      winnerName.append(firstPlayerName)
      matchesWon.append(playerOneWinCount)
    }
    else if playerOneWinCount < playerTwoWinCount {
      winnerName.append(secondPlayerName)
      matchesWon.append(playerTwoWinCount)
    }
    else if playerOneWinCount == playerTwoWinCount {
      winnerName.append("Tie")
      matchesWon.append(playerTwoWinCount)
    }
    playerOneNameArray.append(firstPlayerName)
    playerTwoNameArray.append(secondPlayerName)
    matchesTie.append(tiesCount)
    totalMatches.append(totalScore)
    /*  Saving the value in user defaults.
    First put the value of the array in a Non Mutable Array and the save it with a key */
    let fixedWinnerName = winnerName // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedWinnerName, forKey: "WN")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    let fixedPlayerOne = playerOneNameArray // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedPlayerOne, forKey: "PO")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    let fixedPlayerTwo = playerTwoNameArray // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedPlayerTwo, forKey: "PT")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    let fixedMatchesWon = matchesWon // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedMatchesWon, forKey: "MW")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    let fixedMatchesTie = matchesTie // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedMatchesTie, forKey: "MT")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    let fixedTotalMatches = totalMatches // content of todoArray is put in an immutable array so that it can be saved in user default
    NSUserDefaults.standardUserDefaults().setObject(fixedTotalMatches, forKey: "TM")
    NSUserDefaults.standardUserDefaults().synchronize()
    
    playerOneWinCount = 0
    playerTwoWinCount = 0
    tiesCount = 0
    totalScore = 0
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
